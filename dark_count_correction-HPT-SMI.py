"""
dark count correction will determine the value k in subtracting the
DN values of the image
"""

import rasterio
import numpy as np

raster = 'D:\phlmicrosat\darkcountcorrection\smi660.tif' #input raster


#for SMI, average value of dark border will be the final value of k
def SMIdarkcount(array):
    size = arr.shape
    dn_sum = 0 #sum of the total DNs of dark border
    count = 0 #number of samples
    
    for i in range(size[0]):
        for j in range(size[1]):
            if (i>=8 and j>=2) and (i>=8 and j<=660) and (i<=501 and j<=660) and (i<=501 and j>=2): #if nasa loob ng image window, skip
                break
            else:
                dn_sum = dn_sum + array[i,j]
                count+=1

    return dn_sum/count
    
#for HPT, average value per row will be the values for k (array)  
def HPTdarkcount(array):
    size = arr.shape
    dn_sum = 0 #sum of the total DNs of dark border
    count = 0 #number of samples
    k = []
    
    for i in range(size[0]):
        for j in range(size[1]):
            if (i>=8 and j>=2) and (i>=8 and j<=660) and (i<=501 and j<=660) and (i<=501 and j>=2): #if nasa loob ng image window, skip
                break
            else:
                dn_sum = dn_sum + array[i,j]
                count+=1
        k.append(dn_sum/count)
        dn_sum = 0
        count = 0

    return k

#main
with rasterio.open(raster, 'r+') as r:
    arr = r.read(1)  # read all raster values
    mask = np.zeros(arr.shape) #create empty mask array
    
    #check if HPT or SMI
    if raster.find("SMI")>-1:
        k = SMIdarkcount(arr)
        mask = mask+k #fill mask with dark count correction constant
        
        with rasterio.open(raster+"-DCmask.tif", 'w', **r.profile) as dst:
            dst.write(mask.astype(rasterio.uint16), 1)

            
    elif raster.find("HPT")>-1:
        k = HPTdarkcount(arr)
        for i in range(len(mask)): #fill mask with dark count correction constants
            for j in range(len(mask[0])):
                mask[i][j] = mask[i][j] + k[i]

        with rasterio.open(raster+"-DCmask.tif", 'w', **r.profile) as dst:
            dst.write(mask.astype(rasterio.uint16), 1)    
        
